FROM nginx:1.18-alpine

WORKDIR /var/www

# copy folder build -> nginx root dir var/www
COPY ./build /var/www 

COPY nginx.conf /etc/nginx/nginx.conf  

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]