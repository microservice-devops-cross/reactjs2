import logo from './logo.svg';
import './App.css';
import {add , mul} from '@mazachi/calc';
function App() {
  const result = add(100,1100);
  const result2 = mul(100,55);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Sum is {result} <br/>
          Multiply is {result2}
        </p>
        <p>
         Boom Mazachii Boom Version 2.1
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
